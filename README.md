Kinect Coordinate Mapping
=========================


Purpose
---
The Microsoft Kinect is a motion sensing device, originally designed and used to play video games without a controller. This project uses the version of Kinect for a Windows PC to track positions of 20 joints on the human body as the user moves and output them as X, Y, Z coordinates to a CSV file. Within the CSV file, each data point is timestamped and labeled with its joint type. In the long term, this program will be used to collect data to develop a model for predicting the motions of human subjects. 


How it Works
---
The Kinect has three sensors: a color camera, a depth sensor, and an infrared sensor. The code we began working from incorporated data from both color and depth sensors to map a set of points onto the body. These points are represented as XYZ coordinates, in meters away from the sensor. Data is collected with an expected frame rate of 30 frames per second. 

To test the program's range, we set up the Kinect on a tripod, .94 m above the ground. Using a tape measure, we had users stand 1.85 m away from the camera, and step back approximately .3m with each trial. Joint coordinates became much less stable when the user was more than 4 meters away. This data was then graphed to ensure it corresponded with accurate locations on the user's body.  

Built with C# and X ML in Visual Studio. 


Directions for Use
---
When the window opens, user can enter a User ID and Session ID before or after pressing start, then press the submit ID button to save these as part of the file name. The joint markers should appear immediately after the window opens, but their locations will not be saved until the user presses Start. Data will be recorded until the user presses stop, at which point, it will be found in a file named with the time and date stamp, as well as the previously input IDs. 

Data collection was most effective when the Kinect was 1 meter off the ground and 2-3 meters away from the user, with the Kinect angled slightly down if necessary to ensure the user's entire body was in frame.  

Credits
---
Kinect for Windows SDK Version 1.8 

Kinect 2 CSV and Kinect Coordinate Mapping by Vargos Pterneas, Open Source and available under MIT License https://github.com/LightBuzz/Kinect-2-CSV 

https://github.com/Vangos/kinect-coordinate-mapping 

Kinect Skeleton Basics from Microsoft Development Toolkit (v 1.8) 

Created by Cailin Jordan and Erin O'Rourke with Dr. Qi Mi as part of the First Experiences in Research Program at the University of Pittsburgh. 



﻿using Microsoft.Kinect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//This code is a modified version of the Kinect 2 CSV Output code to run with Kinect v1.8
namespace KinectCSV
{
    public class KinectCSVManager
    {

        public static bool IsRecording { get; protected set; }

        public static string Folder { get; protected set; }

        public string Result { get; protected set; }
        public JointCollection Joints { get; set; }
        public JointType JointType { get; set; }
        public struct CameraSpacePoint { };
        public CameraSpacePoint Position { get; set; }
        private List<DataPoint> datalist = new List<DataPoint>();
        private String userID = null;
        private String sessionID = null;

        public void Start()
        {
            IsRecording = true;
           
        }

        public void Update(Skeleton skeleton)
        {
            if (!IsRecording) return;

            if (skeleton == null) return; //Uncommented while comparing versions
           
            foreach (Joint joint in skeleton.Joints)
            {
                DataPoint dat = new DataPoint(joint.Position.X, joint.Position.Y, joint.Position.Z,
                DateTime.Now.ToString("yyy_MM_dd_HH_mm_ss_ff,"), joint.JointType.ToString());
                datalist.Add(dat);

            }
        }
        public void Submit(String uid, String sid)
        {
            userID = uid;
            sessionID = sid;
        }

        public void Stop()
        {
            IsRecording = false;
            Result = DateTime.Now.ToString("yyy_MM_dd_HH_mm_ss") + "_ " + userID + "_" + sessionID +".csv";
            using (StreamWriter writer = new StreamWriter(Result))
            {
                int counter = 0;
                
                foreach (DataPoint point in datalist)
                    {
                    String output = point.ToString();
                    if (counter % 20 == 0)
                    {
                        output += "\n";
                    }
                    //Debug.Print(output);
                    writer.Write(output);
                    counter++;
                }
            }

        }
    }
    class DataPoint
    {
        float Xpos;
        float Ypos;
        float Zpos;
        String timestamp;
        String JointIndex;// maybe change this to be a string later 
        public DataPoint(float X, float Y, float Z, String time, String Joint)
        {
            Xpos = X;
            Ypos = Y;
            Zpos = Z;
            timestamp = time;
            JointIndex = Joint;
        }
        public String ToString()
        {
            String output = Xpos + "," + Ypos + "," + Zpos + "," + timestamp + "," + JointIndex+",";
            return output;

        }
    }
}
